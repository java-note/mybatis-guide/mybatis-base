package com.laolang.zentao4j;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.laolang.zentao4j.modules.system.dto.SysDictInfoDto;
import com.laolang.zentao4j.modules.system.dto.SysUserDto;
import com.laolang.zentao4j.modules.system.entity.SysDictType;
import com.laolang.zentao4j.modules.system.mapper.SysDictTypeMapper;
import com.laolang.zentao4j.modules.system.mapper.SysUserMapper;
import com.laolang.zentao4j.test.BaseTest;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * 一对一和一对多.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
public class MultiResultTest extends BaseTest {

    private SqlSessionFactory sqlSessionFactory;

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();
        try {
            Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 一对一
     */
    @Test
    public void testOneToOne() throws JsonProcessingException {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            SysUserMapper sysUserMapper = session.getMapper(SysUserMapper.class);
            SysUserDto userDto = sysUserMapper.selectById(1L);
            log.info("sysDictType:{}", object2json(userDto, true));
        }
    }

    @Test
    public void testOneToMany() throws JsonProcessingException {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            SysDictTypeMapper sysDictTypeMapper = session.getMapper(SysDictTypeMapper.class);
            SysDictType params = new SysDictType();
            params.setType("gender");
            params.setGroupCode("system");
            List<SysDictInfoDto> types = sysDictTypeMapper.selectDictInfoList(params);
            log.info("sysDictType:{}", object2json(types, true));
        }
    }

}
