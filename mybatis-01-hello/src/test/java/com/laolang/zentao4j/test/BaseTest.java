package com.laolang.zentao4j.test;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import org.testng.annotations.BeforeClass;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public class BaseTest {

    protected ObjectMapper objectMapper;
    private ObjectWriter prettyWriter;

    @BeforeClass
    public void beforeClass() {
        initJackson();
    }

    private void initJackson() {
        objectMapper = new ObjectMapper();
        /* 数字格式化 */
        SimpleModule numberModule = new SimpleModule();
        // BigDecimal 格式化
        numberModule.addSerializer(BigDecimal.class, new JsonSerializer<BigDecimal>() {
            @Override
            public void serialize(BigDecimal value, JsonGenerator gen,
                SerializerProvider serializers)
                throws IOException {
                DecimalFormat format = new DecimalFormat("0.00");
                gen.writeString(format.format(value));
            }
        });

        // Long 格式化
        numberModule.addSerializer(Long.class, ToStringSerializer.instance);
        numberModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
        numberModule.addSerializer(long.class, ToStringSerializer.instance);
        objectMapper.registerModule(numberModule);


        /* java8 时间格式化 */
        SimpleModule java8TimeModule = new SimpleModule();
        java8TimeModule.addSerializer(LocalDateTime.class,
            new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN)));
        java8TimeModule.addSerializer(LocalDate.class,
            new LocalDateSerializer(
                DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN)));
        java8TimeModule.addSerializer(LocalTime.class,
            new LocalTimeSerializer(
                DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN)));
        java8TimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(
            DateTimeFormatter.ofPattern(DatePattern.NORM_DATETIME_PATTERN)));
        java8TimeModule.addDeserializer(LocalDate.class,
            new LocalDateDeserializer(
                DateTimeFormatter.ofPattern(DatePattern.NORM_DATE_PATTERN)));
        java8TimeModule.addDeserializer(LocalTime.class,
            new LocalTimeDeserializer(
                DateTimeFormatter.ofPattern(DatePattern.NORM_TIME_PATTERN)));
        objectMapper.registerModule(java8TimeModule);

        prettyWriter = objectMapper.writerWithDefaultPrettyPrinter();
    }

    protected String object2json(Object o) throws JsonProcessingException {
        return object2json(o, false);
    }

    protected String object2json(Object o, boolean pretty) throws JsonProcessingException {
        return pretty ? prettyWriter.writeValueAsString(o) : objectMapper.writeValueAsString(o);
    }

}
