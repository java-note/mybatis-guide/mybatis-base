package com.laolang.zentao4j;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.laolang.zentao4j.modules.system.entity.SysDictType;
import com.laolang.zentao4j.modules.system.mapper.SysDictTypeMapper;
import com.laolang.zentao4j.test.BaseTest;
import java.io.IOException;
import java.io.Reader;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
public class CommonTest extends BaseTest {

    private SqlSessionFactory sqlSessionFactory;

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();
        try {
            Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testOne() throws JsonProcessingException {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            SysDictTypeMapper sysDictTypeMapper = session.getMapper(SysDictTypeMapper.class);
            SysDictType sysDictType = sysDictTypeMapper.selectById(1L);
            log.info("sysDictType:{}", object2json(sysDictType, true));
        }
    }
}
