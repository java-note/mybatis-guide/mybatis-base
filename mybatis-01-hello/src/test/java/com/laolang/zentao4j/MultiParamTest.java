package com.laolang.zentao4j;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.laolang.zentao4j.modules.system.dto.SysDictTypeDto;
import com.laolang.zentao4j.modules.system.entity.SysDictType;
import com.laolang.zentao4j.modules.system.mapper.SysDictTypeMapper;
import com.laolang.zentao4j.test.BaseTest;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.collections.Lists;
import org.testng.collections.Maps;

/**
 * 传递多个参数.
 *
 * @author laolang
 * @version 0.1
 */
@Slf4j
public class MultiParamTest extends BaseTest {

    private SqlSessionFactory sqlSessionFactory;

    @BeforeClass
    public void beforeClass() {
        super.beforeClass();
        try {
            Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * `@param` 传参
     */
    @Test
    public void testParam() throws JsonProcessingException {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            SysDictTypeMapper sysDictTypeMapper = session.getMapper(SysDictTypeMapper.class);
            SysDictType sysDictType = sysDictTypeMapper.selectById(1L);
            log.info("sysDictType:{}", object2json(sysDictType, true));
        }
    }

    /**
     * Map 传参
     */
    @Test
    public void testMap() throws JsonProcessingException {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            SysDictTypeMapper sysDictTypeMapper = session.getMapper(SysDictTypeMapper.class);
            Map<String, Object> params = Maps.newHashMap();
            params.put("type", "gender");
            params.put("groupCode", "system");
            List<SysDictType> types = sysDictTypeMapper.selectListByMap(params);
            log.info("sysDictType:{}", object2json(types, true));
        }
    }

    /**
     * 对象 传参
     */
    @Test
    public void testObject() throws JsonProcessingException {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            SysDictTypeMapper sysDictTypeMapper = session.getMapper(SysDictTypeMapper.class);
            SysDictType entity = new SysDictType();
            entity.setType("gender");
            entity.setGroupCode("system");
            List<SysDictType> types = sysDictTypeMapper.selectListByEntity(entity);
            log.info("sysDictType:{}", object2json(types, true));
        }
    }

    /**
     * List 传参
     */
    @Test
    public void testList01() throws JsonProcessingException {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            SysDictTypeMapper sysDictTypeMapper = session.getMapper(SysDictTypeMapper.class);
            List<String> typeList = Lists.newArrayList();
            typeList.add("gender");
            List<SysDictType> types = sysDictTypeMapper.selectListByList01(typeList);
            log.info("sysDictType:{}", object2json(types, true));
        }
    }

    /**
     * JavaBean 包装 List 传参
     */
    @Test
    public void testList02() throws JsonProcessingException {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            SysDictTypeMapper sysDictTypeMapper = session.getMapper(SysDictTypeMapper.class);
            List<String> typeList = Lists.newArrayList();
            typeList.add("gender");
            SysDictTypeDto params = new SysDictTypeDto();
            params.setTypes(typeList);
            List<SysDictType> types = sysDictTypeMapper.selectListByList02(params);
            log.info("sysDictType:{}", object2json(types, true));
        }
    }



}
