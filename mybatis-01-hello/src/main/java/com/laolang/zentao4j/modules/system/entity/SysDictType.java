package com.laolang.zentao4j.modules.system.entity;

import java.time.LocalDateTime;
import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class SysDictType {
    private Long id;

    private String name;
    private String type;
    private String groupCode;
    private String status;
    private Integer deleted = 0;

    private Long createBy;
    private LocalDateTime createTime;
    private Long updateBy;
    private LocalDateTime updateTime;
    private String remark;
    private Integer version;
}
