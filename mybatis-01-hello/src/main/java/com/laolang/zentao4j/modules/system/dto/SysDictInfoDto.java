package com.laolang.zentao4j.modules.system.dto;

import java.util.List;
import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class SysDictInfoDto {

    private Long id;
    private String type;
    private String groupCode;
    private List<SysDictDataBean> datas;

    @Data
    public static class SysDictDataBean{

        private Long id;
        private String type;
        private String groupCode;
        private String value;

    }

}
