package com.laolang.zentao4j.modules.system.dto;

import java.util.List;
import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class SysDictTypeDto {

    private List<String> types;

}
