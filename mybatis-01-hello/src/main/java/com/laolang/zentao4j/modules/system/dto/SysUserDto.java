package com.laolang.zentao4j.modules.system.dto;

import lombok.Data;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
@Data
public class SysUserDto {

    private Long id;
    private String username;
    private DeptBean dept;

    @Data
    public static class DeptBean{
        private Long id;
        private String name;
    }

}
