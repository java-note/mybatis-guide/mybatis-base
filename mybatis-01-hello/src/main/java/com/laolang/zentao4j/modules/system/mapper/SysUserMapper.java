package com.laolang.zentao4j.modules.system.mapper;

import com.laolang.zentao4j.modules.system.dto.SysUserDto;
import org.apache.ibatis.annotations.Param;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysUserMapper {

    SysUserDto selectById(@Param("id")Long id);
}
