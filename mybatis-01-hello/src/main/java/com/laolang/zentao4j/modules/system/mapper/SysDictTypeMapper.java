package com.laolang.zentao4j.modules.system.mapper;

import com.laolang.zentao4j.modules.system.dto.SysDictInfoDto;
import com.laolang.zentao4j.modules.system.dto.SysDictTypeDto;
import com.laolang.zentao4j.modules.system.entity.SysDictType;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

/**
 * xxx.
 *
 * @author laolang
 * @version 0.1
 */
public interface SysDictTypeMapper {

    SysDictType selectById(@Param("id") Long id);

    List<SysDictType> selectListByMap(Map<String,Object> params);

    List<SysDictType> selectListByEntity(@Param("entity") SysDictType entity);

    List<SysDictType> selectListByList01(@Param("types") List<String> types);

    List<SysDictType> selectListByList02(@Param("params") SysDictTypeDto dto);

    List<SysDictInfoDto> selectDictInfoList(@Param("params") SysDictType params);

}
